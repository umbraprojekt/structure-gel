"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MissingKeyBehaviour;
(function (MissingKeyBehaviour) {
    MissingKeyBehaviour["IGNORE"] = "IGNORE";
    MissingKeyBehaviour["THROW"] = "THROW";
    MissingKeyBehaviour["DEFAULT"] = "DEFAULT";
})(MissingKeyBehaviour = exports.MissingKeyBehaviour || (exports.MissingKeyBehaviour = {}));
;

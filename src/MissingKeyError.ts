export class MissingKeyError extends Error {

	constructor(message?: string) {
		super(message);
	}
}

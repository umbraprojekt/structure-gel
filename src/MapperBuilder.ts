import { FieldMapper } from "./FieldMapper";
import { Mapper } from "./Mapper";
import { MapperImpl } from "./MapperImpl";
import { MissingKeyBehaviour } from "./MissingKeyBehaviour";
import { MissingKeyError } from "./MissingKeyError";
import { defaultOptions, Options } from "./Options";
import { TransformFunction } from "./TransformFunction";
import { TranslateFunction } from "./TranslateFunction";
import { UnknownCollectionError } from "./UnknownCollectionError";
import extend = require("extend");

const prepareKeys = (keys: string | string[] | {[key: string]: string}): {[key: string]: string} => {
	if (typeof keys === "string") {
		const output = {} as any;
		output[keys] = keys;
		return output;
	}
	if (Array.isArray(keys)) {
		const output = {} as any;
		keys.forEach(key => output[key] = key);
		return output;
	}
	return keys;
};

const filterKeys = (keys: string[], input: any, options: Options): string[] => {
	if ("default" in options) {
		return keys;
	}
	if (options.onMissingKey === MissingKeyBehaviour.THROW) {
		keys.forEach(key => assertKeyExists(key, input));
	}

	return keys.filter(key => key in input);
};

const assertKeyExists = (key: string, input: any): void => {
	if (!(key in input)) {
		throw new MissingKeyError(`The key "${key}" does not exist on the input object.`);
	}
}

export class MapperBuilder<T, U> {

	private _fieldMappers: FieldMapper<T, U>[] = [];
	private _options: Options = extend({}, defaultOptions);
	private _outputFactory: () => U = () => ({} as any);

	public throwOnMissing(): MapperBuilder<T, U> {
		this._options.onMissingKey = MissingKeyBehaviour.THROW;
		return this;
	}

	public ignoreMissing(): MapperBuilder<T, U> {
		this._options.onMissingKey = MissingKeyBehaviour.IGNORE;
		return this;
	}

	public copy(keys: string | string[] | {[key: string]: string}, options: Options = {}): MapperBuilder<T, U> {
		return this.translate(keys, inputValue => inputValue, options);
	}

	public transform(transformFunc: TransformFunction<T, U>): MapperBuilder<T, U> {
		this._fieldMappers.push({
			map(input: T, output: U): void | Promise<void> {
				return transformFunc(input, output);
			}
		});
		return this;
	}

	public translate(keys: string | string[] | {[key: string]: string}, translateFunc: TranslateFunction<T, U>, options: Options = {}): MapperBuilder<T, U> {
		const preparedKeys = prepareKeys(keys);
		const preparedOptions: Options = extend({}, this._options, options);
		const validate = options.validate ? options.validate : (v: any) => {};
		this._fieldMappers.push({
			map(input: any, output: any): void | Promise<void> {
				const promises: Promise<void>[] = [];
				filterKeys(Object.keys(preparedKeys), input, preparedOptions).forEach(key => {
					if (!(key in input)) {
						output[preparedKeys[key]] = preparedOptions.default;
						return;
					}

					validate(input[key]);
					const translated = translateFunc(input[key]);
					if (translated instanceof Promise) {
						promises.push(translated.then(value => {
							output[preparedKeys[key]] = value;
						}));
						return;
					}
					output[preparedKeys[key]] = translated;
				});
				if (promises.length) {
					return Promise.all(promises).then();
				}
			}
		});
		return this;
	}

	public translateEach(keys: string | string[] | {[key: string]: string}, translateFunc: TranslateFunction<any, any>, options: Options = {}): MapperBuilder<T, U> {
		const preparedKeys = prepareKeys(keys);
		const preparedOptions: Options = extend({}, this._options, options);
		const validate = options.validate ? options.validate : (v: any) => { };
		this._fieldMappers.push({
			map(input: any, output: any): void | Promise<void> {
				const promises: Promise<void>[] = [];
				filterKeys(Object.keys(preparedKeys), input, preparedOptions).forEach(key => {
					if (!(key in input)) {
						output[preparedKeys[key]] = preparedOptions.default;
						return;
					}

					if (Array.isArray(input[key])) {
						output[preparedKeys[key]] = [];
						input[key].forEach((item: any, idx: number) => {
							validate(item);
							const translated = translateFunc(item);
							if (translated instanceof Promise) {
								promises.push(translated.then(value => {
									output[preparedKeys[key]][idx] = value;
								}));
								return;
							}
							output[preparedKeys[key]].push(translated);
						});
					} else if (input[key] instanceof Set) {
						output[preparedKeys[key]] = new Set();
						input[key].forEach((item: any) => {
							validate(item);
							const translated = translateFunc(item);
							if (translated instanceof Promise) {
								promises.push(translated.then(value => {
									output[preparedKeys[key]].add(value);
								}));
								return;
							}
							output[preparedKeys[key]].add(translated);
						});
					} else if (input[key] instanceof Map || input[key] instanceof WeakMap) {
						output[preparedKeys[key]] = new Map();
						input[key].forEach((item: any, idx: any) => {
							validate(item);
							const translated = translateFunc(item);
							if (translated instanceof Promise) {
								promises.push(translated.then(value => {
									output[preparedKeys[key]].set(idx, value);
								}));
								return;
							}
							output[preparedKeys[key]].set(idx, translated);
						});
					} else {
						throw new UnknownCollectionError(`The key "${key}" is not a collection or an unsupported collection type.`);
					}
				});
				if (promises.length) {
					return Promise.all(promises).then();
				}
			}
		});
		return this;
	}

	public map(keys: string | string[] | {[key: string]: string}, mapper: Mapper<any, any>, options: Options = {}): MapperBuilder<T, U> {
		return this.translate(keys, inputValue => mapper.map(inputValue), options);
	}

	public mapEach(keys: string | string[] | {[key: string]: string}, mapper: Mapper<any, any>, options: Options = {}): MapperBuilder<T, U> {
		return this.translateEach(keys, inputValue => mapper.map(inputValue), options);
	}

	public outputType(ctor: any): MapperBuilder<T, U> {
		this._outputFactory = () => Object.setPrototypeOf({}, ctor.prototype);
		return this;
	}

	public outputFactory(factory: () => U): MapperBuilder<T, U> {
		this._outputFactory = factory;
		return this;
	}

	public build(mapperName?: string): Mapper<T, U> {
		return new MapperImpl(this._fieldMappers, this._outputFactory, mapperName);
	}
}

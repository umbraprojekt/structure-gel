"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UnknownCollectionError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.UnknownCollectionError = UnknownCollectionError;

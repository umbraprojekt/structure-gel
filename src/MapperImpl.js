"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const extend = require("extend");
const MissingKeyError_1 = require("./MissingKeyError");
const UnknownCollectionError_1 = require("./UnknownCollectionError");
class MapperImpl {
    constructor(mappings, output, mapperName) {
        this._fieldMappers = [];
        this._fieldMappers = mappings;
        this._outputFactory = output;
        this._mapperName = mapperName;
    }
    map(input) {
        const output = this._outputFactory();
        const extendedInput = extend(true, {}, input);
        const promises = [];
        for (const fieldMapper of this._fieldMappers) {
            let fieldMapperOutput;
            try {
                fieldMapperOutput = fieldMapper.map(extendedInput, output);
            }
            catch (e) {
                if (this._mapperName) {
                    e.message = `${this._mapperName}: ${e.message}`;
                }
                if (e instanceof MissingKeyError_1.MissingKeyError) {
                    throw new MissingKeyError_1.MissingKeyError(e.message);
                }
                else if (e instanceof UnknownCollectionError_1.UnknownCollectionError) {
                    throw new UnknownCollectionError_1.UnknownCollectionError(e.message);
                }
                else {
                    throw e;
                }
            }
            if (fieldMapperOutput instanceof Promise) {
                promises.push(fieldMapperOutput);
            }
        }
        return promises.length ? Promise.all(promises).then(() => output) : output;
    }
}
exports.MapperImpl = MapperImpl;

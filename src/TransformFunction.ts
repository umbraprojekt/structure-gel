export type TransformFunction<T, U> = (inputObject: T, outputObject: U) => void | Promise<void>;

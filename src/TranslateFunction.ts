export type TranslateFunction<T, U> = (inputValue: any) => any | Promise<any>;

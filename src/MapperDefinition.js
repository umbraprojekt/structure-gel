"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MapperDefinition {
    constructor() {
        this._datamap = new Map();
    }
    omit(keys) {
        if (typeof keys === "string") {
            keys = [keys];
        }
        keys.forEach(key => {
            if (this._datamap.has(key)) {
                this._datamap.delete(key);
            }
        });
        return this;
    }
    copy(keys) {
        if (typeof keys === "string") {
            keys = [keys];
        }
        keys.forEach(key => this._datamap.set(key, true));
        return this;
    }
    transform(key, transformFunc) {
        this._datamap.set(key, transformFunc);
        return this;
    }
    map(key, mapper) {
        this._datamap.set(key, (inputValue, outputObject) => {
            outputObject[key] = mapper.map(inputValue);
        });
        return this;
    }
    [Symbol.iterator]() {
        return this._datamap.entries();
    }
}
exports.MapperDefinition = MapperDefinition;

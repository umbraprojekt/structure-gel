"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MapperImpl_1 = require("./MapperImpl");
const MissingKeyBehaviour_1 = require("./MissingKeyBehaviour");
const MissingKeyError_1 = require("./MissingKeyError");
const Options_1 = require("./Options");
const UnknownCollectionError_1 = require("./UnknownCollectionError");
const extend = require("extend");
const prepareKeys = (keys) => {
    if (typeof keys === "string") {
        const output = {};
        output[keys] = keys;
        return output;
    }
    if (Array.isArray(keys)) {
        const output = {};
        keys.forEach(key => output[key] = key);
        return output;
    }
    return keys;
};
const filterKeys = (keys, input, options) => {
    if ("default" in options) {
        return keys;
    }
    if (options.onMissingKey === MissingKeyBehaviour_1.MissingKeyBehaviour.THROW) {
        keys.forEach(key => assertKeyExists(key, input));
    }
    return keys.filter(key => key in input);
};
const assertKeyExists = (key, input) => {
    if (!(key in input)) {
        throw new MissingKeyError_1.MissingKeyError(`The key "${key}" does not exist on the input object.`);
    }
};
class MapperBuilder {
    constructor() {
        this._fieldMappers = [];
        this._options = extend({}, Options_1.defaultOptions);
        this._outputFactory = () => ({});
    }
    throwOnMissing() {
        this._options.onMissingKey = MissingKeyBehaviour_1.MissingKeyBehaviour.THROW;
        return this;
    }
    ignoreMissing() {
        this._options.onMissingKey = MissingKeyBehaviour_1.MissingKeyBehaviour.IGNORE;
        return this;
    }
    copy(keys, options = {}) {
        return this.translate(keys, inputValue => inputValue, options);
    }
    transform(transformFunc) {
        this._fieldMappers.push({
            map(input, output) {
                return transformFunc(input, output);
            }
        });
        return this;
    }
    translate(keys, translateFunc, options = {}) {
        const preparedKeys = prepareKeys(keys);
        const preparedOptions = extend({}, this._options, options);
        const validate = options.validate ? options.validate : (v) => { };
        this._fieldMappers.push({
            map(input, output) {
                const promises = [];
                filterKeys(Object.keys(preparedKeys), input, preparedOptions).forEach(key => {
                    if (!(key in input)) {
                        output[preparedKeys[key]] = preparedOptions.default;
                        return;
                    }
                    validate(input[key]);
                    const translated = translateFunc(input[key]);
                    if (translated instanceof Promise) {
                        promises.push(translated.then(value => {
                            output[preparedKeys[key]] = value;
                        }));
                        return;
                    }
                    output[preparedKeys[key]] = translated;
                });
                if (promises.length) {
                    return Promise.all(promises).then();
                }
            }
        });
        return this;
    }
    translateEach(keys, translateFunc, options = {}) {
        const preparedKeys = prepareKeys(keys);
        const preparedOptions = extend({}, this._options, options);
        const validate = options.validate ? options.validate : (v) => { };
        this._fieldMappers.push({
            map(input, output) {
                const promises = [];
                filterKeys(Object.keys(preparedKeys), input, preparedOptions).forEach(key => {
                    if (!(key in input)) {
                        output[preparedKeys[key]] = preparedOptions.default;
                        return;
                    }
                    if (Array.isArray(input[key])) {
                        output[preparedKeys[key]] = [];
                        input[key].forEach((item, idx) => {
                            validate(item);
                            const translated = translateFunc(item);
                            if (translated instanceof Promise) {
                                promises.push(translated.then(value => {
                                    output[preparedKeys[key]][idx] = value;
                                }));
                                return;
                            }
                            output[preparedKeys[key]].push(translated);
                        });
                    }
                    else if (input[key] instanceof Set) {
                        output[preparedKeys[key]] = new Set();
                        input[key].forEach((item) => {
                            validate(item);
                            const translated = translateFunc(item);
                            if (translated instanceof Promise) {
                                promises.push(translated.then(value => {
                                    output[preparedKeys[key]].add(value);
                                }));
                                return;
                            }
                            output[preparedKeys[key]].add(translated);
                        });
                    }
                    else if (input[key] instanceof Map || input[key] instanceof WeakMap) {
                        output[preparedKeys[key]] = new Map();
                        input[key].forEach((item, idx) => {
                            validate(item);
                            const translated = translateFunc(item);
                            if (translated instanceof Promise) {
                                promises.push(translated.then(value => {
                                    output[preparedKeys[key]].set(idx, value);
                                }));
                                return;
                            }
                            output[preparedKeys[key]].set(idx, translated);
                        });
                    }
                    else {
                        throw new UnknownCollectionError_1.UnknownCollectionError(`The key "${key}" is not a collection or an unsupported collection type.`);
                    }
                });
                if (promises.length) {
                    return Promise.all(promises).then();
                }
            }
        });
        return this;
    }
    map(keys, mapper, options = {}) {
        return this.translate(keys, inputValue => mapper.map(inputValue), options);
    }
    mapEach(keys, mapper, options = {}) {
        return this.translateEach(keys, inputValue => mapper.map(inputValue), options);
    }
    outputType(ctor) {
        this._outputFactory = () => Object.setPrototypeOf({}, ctor.prototype);
        return this;
    }
    outputFactory(factory) {
        this._outputFactory = factory;
        return this;
    }
    build(mapperName) {
        return new MapperImpl_1.MapperImpl(this._fieldMappers, this._outputFactory, mapperName);
    }
}
exports.MapperBuilder = MapperBuilder;

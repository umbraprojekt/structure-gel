export class UnknownCollectionError extends Error {

	constructor(message?: string) {
		super(message);
	}
}

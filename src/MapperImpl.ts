import * as extend from "extend";
import { FieldMapper } from "./FieldMapper";
import { Mapper } from "./Mapper";
import { MissingKeyError } from "./MissingKeyError";
import { UnknownCollectionError } from "./UnknownCollectionError";

export class MapperImpl<T, U> implements Mapper<T, U> {

	private readonly _fieldMappers: FieldMapper<T, U>[] = [];
	private readonly _outputFactory: () => U;
	private readonly _mapperName?: string;

	public constructor (mappings: FieldMapper<T, U>[], output: () => U, mapperName?: string) {
		this._fieldMappers = mappings;
		this._outputFactory = output;
		this._mapperName = mapperName;
	}

	public map(input: T): U | Promise<U> {
		const output: U = this._outputFactory();
		const extendedInput: T = extend(true, {}, input);
		const promises: Promise<U>[] = [];

		for (const fieldMapper of this._fieldMappers) {
			let fieldMapperOutput: any;
			try {
				fieldMapperOutput = fieldMapper.map(extendedInput, output);
			} catch (e) {
				if (this._mapperName) {
					e.message = `${this._mapperName}: ${e.message}`;
				}
				if (e instanceof MissingKeyError) {
					throw new MissingKeyError(e.message);
				} else if (e instanceof UnknownCollectionError) {
					throw new UnknownCollectionError(e.message);
				} else {
					throw e;
				}
			}
			if (fieldMapperOutput instanceof Promise) {
				promises.push(fieldMapperOutput as any);
			}
		}

		return promises.length ? Promise.all(promises).then(() => output) : output;
	}
}

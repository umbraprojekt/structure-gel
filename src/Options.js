"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MissingKeyBehaviour_1 = require("./MissingKeyBehaviour");
exports.defaultOptions = {
    onMissingKey: MissingKeyBehaviour_1.MissingKeyBehaviour.THROW
};

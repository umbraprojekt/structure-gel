export declare class MissingKeyError extends Error {
    constructor(message?: string);
}

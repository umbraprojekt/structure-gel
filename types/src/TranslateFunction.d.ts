export declare type TranslateFunction<T, U> = (inputValue: any) => any | Promise<any>;

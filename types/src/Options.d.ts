import { MissingKeyBehaviour } from "./MissingKeyBehaviour";
export interface Options {
    onMissingKey?: MissingKeyBehaviour;
    default?: any;
    validate?: (value: any) => void;
}
export declare const defaultOptions: Options;

export interface Mapper<T, U> {
    map(input: T): U | Promise<U>;
}

import { Mapper } from "./Mapper";
export declare class Mappers {
    private static readonly _mappers;
    static register(key: string, mapper: Mapper<any, any>): void;
    static get(key: string): Mapper<any, any>;
}

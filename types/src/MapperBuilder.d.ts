import { Mapper } from "./Mapper";
import { Options } from "./Options";
import { TransformFunction } from "./TransformFunction";
import { TranslateFunction } from "./TranslateFunction";
export declare class MapperBuilder<T, U> {
    private _fieldMappers;
    private _options;
    private _outputFactory;
    throwOnMissing(): MapperBuilder<T, U>;
    ignoreMissing(): MapperBuilder<T, U>;
    copy(keys: string | string[] | {
        [key: string]: string;
    }, options?: Options): MapperBuilder<T, U>;
    transform(transformFunc: TransformFunction<T, U>): MapperBuilder<T, U>;
    translate(keys: string | string[] | {
        [key: string]: string;
    }, translateFunc: TranslateFunction<T, U>, options?: Options): MapperBuilder<T, U>;
    translateEach(keys: string | string[] | {
        [key: string]: string;
    }, translateFunc: TranslateFunction<any, any>, options?: Options): MapperBuilder<T, U>;
    map(keys: string | string[] | {
        [key: string]: string;
    }, mapper: Mapper<any, any>, options?: Options): MapperBuilder<T, U>;
    mapEach(keys: string | string[] | {
        [key: string]: string;
    }, mapper: Mapper<any, any>, options?: Options): MapperBuilder<T, U>;
    outputType(ctor: any): MapperBuilder<T, U>;
    outputFactory(factory: () => U): MapperBuilder<T, U>;
    build(mapperName?: string): Mapper<T, U>;
}

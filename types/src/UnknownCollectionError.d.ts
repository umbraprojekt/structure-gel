export declare class UnknownCollectionError extends Error {
    constructor(message?: string);
}

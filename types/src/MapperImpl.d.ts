import { FieldMapper } from "./FieldMapper";
import { Mapper } from "./Mapper";
export declare class MapperImpl<T, U> implements Mapper<T, U> {
    private readonly _fieldMappers;
    private readonly _outputFactory;
    private readonly _mapperName?;
    constructor(mappings: FieldMapper<T, U>[], output: () => U, mapperName?: string);
    map(input: T): U | Promise<U>;
}

export declare enum MissingKeyBehaviour {
    IGNORE = "IGNORE",
    THROW = "THROW",
    DEFAULT = "DEFAULT"
}

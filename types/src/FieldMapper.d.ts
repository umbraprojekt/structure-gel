export interface FieldMapper<T, U> {
    map(input: T, output: U): void | Promise<void>;
}

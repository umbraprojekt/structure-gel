# Structure Gel

Structure Gel is a simple tool that maps data between different data structures (objects). Values can be copied verbatim, omitted, transformed using a transform function or mapped using another mapper.

## Installation

`npm install --save structure-gel`

## API

* `class MapperBuilder<Input, Output>` - used for creating mappers that map from `Input` to `Output` type.
    * `MapperBuilder.copy(keys: string | string[] | {[key: string]: string}, options?: Options)` - copy keys verbatim
    * `MapperBuilder.translate(keys: string | string[] | {[key: string]: string}, translateFunc: (value: any) => any, options?: Options)` - apply a `translateFunc` to every value specified by the `keys` argument.
    * `MapperBuilder.translateEach(keys: string | string[] | {[key: string]: string}, options?: Options)` - apply a `translateFunc` to every value in collections specified by the `keys` argument
    * `MapperBuilder.transform(transformFunc: (in: Input, out: Output) => void)` - rewrite the input object in a non-standard way
    * `MapperBuilder.map(keys: string | string[] | {[key: string]: string}, mapper: Mapper<any, any>, options?: Options)` - translate values using a mapper instead of a translate function
    * `MapperBuilder.mapEach(keys: string | string[] | {[key: string]: string}, mapper: Mapper<any, any>, options?: Options)` - translate values of collections specified by the `keys` argument usin a mapper instead of a translate function
    * `MapperBuilder.ignoreMissing()` - ignore missing object keys in subsequent method calls
    * `MapperBuilder.throwOnMissing()` - throw on missing object keys in subsequent method calls
    * `MapperBuilder.outputType(ctor: any)` - specify the output constructor. Marked as `any` in order to allow constructors with any number of arguments.
    * `MapperBuilder.outputFactory(factory: () => Output)` - define how to construct the output object
    * `MapperBuilder.build(mapperName?: string)` - build the `Mapper<Input, Output>` object
* `interface Options` - options interface
    * `onMissingKey?: "THROW" | "IGNORE" | "DEFAULT"` - specify the behaviour when a missing key is encountered in the input
    * `default?: any` - provide a default value for missing keys; overrides `onMissingKey`
    * `validate?: (value: any) => void` - validator function applied to the specified values

## Usage

### Basic

This is the most basic usage of a mapper. Using the `MapperBuilder` object, a mapper with the desired behaviour can be built. In this particular case, the mapper will just copy the fields verbatim from the input to the output. The produced output has the same values as the input, but is not the same instance.

```.typescript
import { MapperBuilder } from "structure-gel";
import { expect } from "mocha";

describe("Mapper", () => {
	it("maps values", () => {
		// given
		const mapper = new MapperBuilder()
			.copy(["foo", "bar"])
			.build();

		const input = {
			foo: "a string",
			bar: 123
		};
		const expectedOutput = input;

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
		expect(output).to.not.equal(input);
	});
});
```

Usage with `require()` is also possible:

```.js
const MapperBuilder = require("structure-gel").MapperBuilder;
```

### Specifying keys to process

Each method that processes object properties by key (`copy`, `translate`, `translateEadh`, `map`, `mapEach`) take the keys to process as their first argument. The type of the argument is `keys: string | string[] | {[key: string]: string}`. This means that it's possible to pass in the keys to process as:

* a single string, e.g. `.copy("title")`; in this case, only a single key will be processed,
* an array of keys, e.g. `.copy(["title", "body"])`; in this case, multiple keys will be processed,
* an object where the keys represent the keys of the input object and the values represent the corresponding keys of the output object, e.g. `.copy({ title: "article_title", body: "body" })`; in this case, any number of keys will be processed and additionally renamed. In the provided example, the key `"title"` will be copied from the input object to the property named `"article_title"` on the output. Providing the same key and value will result in copying a property value without renaming the property.

### Copying values

This is the most basic form of mapping. Fields from the input object are copied verbatim to the output. This is useful for simple fields of primitive types.

The `copy()` method of the builder can either accept a single field name or an array of field names.

```.typescript
// given
const mapper = new MapperBuilder()
	.copy(["foo", "bar"])
	.copy("qux")
	.build();

const input = {
	foo: "a string",
	bar: 123,
	qux: "another string"
};
const expectedOutput = input;

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

### Translating values

Translating values means changing the value of a field and assigining it to a field on the output object with the same name. For example, a field containing a `Date` object can be translated into a string representation of the date for easier persistence.

The `translate()` method of the builder can either accept a single field name or an array of field names.

```.typescript
// given
const mapper = new MapperBuilder()
	.translate(["startDate", "endDate"], date => date.toISOString())
	.translate("name", name => name.toUpperCase())
	.build();

const startDate = "2020-01-01T00:00:00.000Z";
const endDate = "2020-01-02T00:00:00.000Z";
const input = {
	startDate: new Date(startDate),
	endDate: new Date(endDate),
	name: "uppercase"
};
const expectedOutput = {
	startDate,
	endDate,
	name: "UPPERCASE"
};

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

The translate callback can be sync or async.

### Translating collections

It's possible to translate values inside a collection (`Array`, `Set` or `Map`). It works the same way as if the method `Array.prototype.map()` was called, with the exception that the handler function can either be sync or async, returning either the translated value or a promise containing it. In case of `Map` objects, keys are retained and values are translated. In case of arrays, value ordering is also retained. Other collection types, including `WeakSet` and `WeakMap`, are not supported and will result in throwing an `UnknownCollectionError`.

The translate handler will be called in sequence, receiving each item's value as input each time.

```.typescript
// given
const mapper = new MapperBuilder()
	.translateEach(["numbers", "alsoNumbers"], num => num.toString())
	.translateEach("letters", letter => letter.toLowerCase())
	.build();

const input = {
	numbers: [1, 1, 2, 3, 5, 8, 13],
	alsoNumbers: [1, 2, 4, 8, 16, 32],
	letters: ["A", "B", "C"]
};
const expectedOutput = {
	numbers: ["1", "1", "2", "3", "5", "8", "13"],
	alsoNumbers: ["1", "2", "4", "8", "16", "32"],
	letters: ["a", "b", "c"]
};

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

### Transforming objects

Transforming objects is the most complex mapping type. It is used when either multiple fields need to be taken into account for the purpose of the mapping or the field name changes between the input and output objects.

The `transform()` method of the builder does not accept field names, only the transform callback function.

```.typescript
// given
const mapper = new MapperBuilder()
	.transform((input: any, output: any) => {
		output.quantity = [input.amount, input.unit].join(" ");
	})
	.build();

const input = {
	amount: 10,
	unit: "KILOGRAM"
};
const expectedOutput = {
	quantity: "10 KILOGRAM"
};

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

The transform callback can be sync or async.

### Mapping values using dependent mappers

Many objects, especially deeply nested, will often require repeating the same mapping multiple times. Instead of adding multiple translations, it is possible to define a separate mapper and reuse it multiple times.

It is also possible to use the additional mapper inside a transform to work on the root object.

The `map()` method of the builder can either accept a single field name or an array of field names.

```.typescript
const personMapper = new MapperBuilder()
	.transform((input: any, output: any) => {
		output.name = [input.firstName, input.lastName].join(" ");
	})
	.build();
const mapper = new MapperBuilder()
	.copy("name")
	.map(["father", "mother"], personMapper)
	.map("sister", personMapper)
	.build();

const input = {
	name: "Stewie",
	father: {
		firstName: "Peter",
		lastName: "Griffin"
	},
	mother: {
		firstName: "Lois",
		lastName: "Griffin"
	},
	sister: {
		firstName: "Meg",
		lastName: "Griffin"
	}
};
const expectedOutput = {
	name: "Stewie",
	father: {
		name: "Peter Griffin"
	},
	mother: {
		name: "Lois Griffin"
	},
	sister: {
		name: "Meg Griffin"
	}
};

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

### Mapping collections using dependent mappers

It is possible to use a dependent mapper on a collection (`Array`, `Set` or `Map`). Works in the same manner as translating collections.

```.typescript
// given
const minMaxMapper = new MapperBuilder()
	.translate(["min", "max"], value => value.toString())
	.build();
const mapper = new MapperBuilder()
	.mapEach(["gravityRanges", "ibuRanges"], minMaxMapper)
	.mapEach("srmRanges", minMaxMapper)
	.build();

const input = {
	gravityRanges: [{ min: 1040, max: 1060 }, { min: 1080, max: 1100 }],
	ibuRanges: [{ min: 20, max: 40 }, { min: 60, max: 100 }],
	srmRanges: [{ min: 2, max: 6 }, { min: 35, max: 40 }]
};
const expectedOutput = {
	gravityRanges: [{ min: "1040", max: "1060" }, { min: "1080", max: "1100" }],
	ibuRanges: [{ min: "20", max: "40" }, { min: "60", max: "100" }],
	srmRanges: [{ min: "2", max: "6" }, { min: "35", max: "40" }]
};

// when
const output = mapper.map(input);

// then
expect(output).to.deep.equal(expectedOutput);
```

### Mandatory and optional fields

Fields that the mapper operates on are mandatory by default, that is, an error will be thrown if trying to operate on a field that is missing in the input object:

```.typescript
const mapper = new MapperBuilder()
	.copy("foo")
	.build();

expect(() => mapper.map({}))
	.to.throw(MissingKeyError, 'The key "foo" does not exist on the input object.');
```

In order to explicitly mark fields as optional or mandatory, two methods exist on the builder:

* `MapperBuilder#ignoreMissing()`
* `MapperBuilder#throwOnMissing()`

When called, all subsequent methods operating on fields will adjust their behaviour accordingly.

```.typescript
const mapper = new MapperBuilder()
	.copy("foo") // mandatory; throw if the key is missing
	.ignoreMissing()
	.copy("bar") // optional; ignore if the key is missing
	.copy("qux") // optional; ignore if the key is missing
	.throwOnMissing()
	.copy("bat") // mandatory; throw if the key is missing
	.copy("man") // mandatory; throw if the key is missing
	.build();
```

### Passing in options

It's possible to pass in an additional `Options` object as the last argument of the following methods:

* `copy`
* `translate`
* `translateEach`
* `map`
* `mapEach`

The options will be used to override the current missing key behaviour and provide additional functionality: validation, and default value for missing keys.

```.typescript
const mapper = new MapperBuilder()
	.copy("foo", { default: "bar" }) // will use default value of `input.foo` doesn't exist
	.translate("bar", val => val, { validate: (val) => typeof val === "string" }) // will run validation on the key
	.build();
```

In case of `*Each` methods, the settings will be applied to each value in the processed collections, e.g. is mapping an array with 10 values, the validation will be run 10 times.

### Passing in the output object type

If the output object needs to be an instance of a specific type (e.g. in order to use its methods), the mapper needs to be made aware of that. One way is to simply pass the object type to the `outputType()` method of the builder:

```.typescript
const mapper = new MapperBuilder()
	.copy(["foo", "bar"])
	.outputType(OutputObject)
	.build();
```

Note that the argument type

### Passing in the output object factory

If the output object needs to be an instance of a specific type, but creating it requires some additional work, e.g. calling a constructor with some arguments, a factory method may also be passed to the `outputFactory()` method of the builder:

```.typescript
const mapper = new MapperBuilder()
	.copy(["foo", "bar"])
	.outputFactory(() => new OutputObject(123))
	.build();
```

### Passing in mapper name

Mappers can optionally be named. It serves no purpose in the mapping itself, but rather helps in diagnosing problems. For instance, when an error is thrown, the stack trace will only indicate issues with `MapperImpl` or `MapperBuilder`, making it difficult to trace the error back to the actual mapper instance, especially if using a mapper with multiple dependent mappers. To mitigate this, it's possible to pass in the mapper name to the `build()` method od the builder. The passed in name will be prepended to all errors thrown by the mapper. E.g.:

```.typescript
// given
const mapper = new MapperBuilder()
	.copy("foo")
	.build("FooMapper");

// when & then
expect(() => mapper.map({}))
	.to.throw(MissingKeyError, "FooMapper: The key \"foo\" does not exist on the input object.");
```

import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import * as sinonChai from "sinon-chai";
import { MapperBuilder } from "../src";
import { MissingKeyError } from "../src/MissingKeyError";

chai.use(sinonChai);

describe("MapperImpl", () => {
	it("should copy values", () => {
		// given
		const input = {
			a: "a",
			b: 1,
			c: ["foo", "bar"]
		};
		const expectedOutput = input;
		const mapper = new MapperBuilder()
			.copy(["a", "b"])
			.copy("c")
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
		expect(output).to.not.equal(input);
	});

	it("should copy and rename values", () => {
		// given
		const input = {
			a: "foo",
			b: "bar"
		};
		const expectedOutput = {
			b: "foo",
			a: "bar"
		};
		const mapper = new MapperBuilder()
			.copy({ a: "b", b: "a" })
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should omit values", () => {
		// given
		const input = {
			a: "a",
			b: 1,
			c: ["foo", "bar"]
		};
		const expectedOutput = {
			b: 1,
			c: ["foo", "bar"]
		};
		const mapper = new MapperBuilder()
			.copy(["b", "c"])
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should transform values", () => {
		// given
		const date = new Date();
		const dateString = date.toUTCString();
		const input = {
			a: {
				id: "FOO",
				value: "somevalue"
			},
			b: date
		};
		const expectedOutput = {
			a: "FOO",
			date: dateString
		};
		const mapper = new MapperBuilder()
			.transform((inputObject: any, outputObject: any) => {
				outputObject.a = inputObject.a.id;
				outputObject.date = inputObject.b.toUTCString();
			})
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should transform multifield values", () => {
		// given
		const input = {
			amount: 10,
			unit: "KILOGRAM"
		};
		const expectedOutput = {
			value: "10 KILOGRAM"
		};
		const mapper = new MapperBuilder()
			.transform((inputObject: any, outputObject: any) => {
				outputObject.value = [inputObject.amount.toString(), inputObject.unit].join(" ");
			})
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should map values", () => {
		// given
		const input = {
			father: {
				firstName: "Peter",
				lastName: "Griffin"
			},
			mother: {
				firstName: "Lois",
				lastName: "Griffin"
			},
			sister: {
				firstName: "Meg",
				lastName: "Griffin"
			}
		};
		const expectedOutput = {
			father: {
				name: "Peter Griffin"
			},
			mother: {
				name: "Lois Griffin"
			},
			sister: {
				name: "Meg Griffin"
			}
		};
		const personMapper = new MapperBuilder()
			.transform((input: any, output: any) => {
				output.name = [input.firstName, input.lastName].join(" ");
			})
			.build();
		const mapper = new MapperBuilder()
			.map(["father", "mother"], personMapper)
			.map("sister", personMapper)
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should map and rename values", () => {
		// given
		const input = {
			father: {
				firstName: "Peter",
				lastName: "Griffin"
			},
			mother: {
				firstName: "Lois",
				lastName: "Griffin"
			},
			sister: {
				firstName: "Meg",
				lastName: "Griffin"
			}
		};
		const expectedOutput = {
			dad: {
				name: "Peter Griffin"
			},
			mum: {
				name: "Lois Griffin"
			},
			sis: {
				name: "Meg Griffin"
			}
		};
		const personMapper = new MapperBuilder()
			.transform((input: any, output: any) => {
				output.name = [input.firstName, input.lastName].join(" ");
			})
			.build();
		const mapper = new MapperBuilder()
			.map({ father: "dad", mother: "mum", sister: "sis" }, personMapper)
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should allow asynchronous execution", async () => {
		// given
		const input = {
			a: "a",
			b: "b"
		};
		const expectedOutput = input;
		const mapper = new MapperBuilder()
			.transform((inputObject: any, outputObject: any) => {
				return new Promise((resolve) => {
					setTimeout(() => {
						outputObject.a = inputObject.a;
						outputObject.b = inputObject.b;
						resolve();
					}, 10);
				});
			})
			.build();

		// when
		const output = await mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
		expect(output).to.not.equal(input);
	});

	it("should translate values", () => {
		const startDate = "2020-01-01T00:00:00.000Z";
		const endDate = "2020-01-02T00:00:00.000Z";
		const input = {
			startDate: new Date(startDate),
			endDate: new Date(endDate),
			name: "uppercase"
		};
		const expectedOutput = {
			startDate,
			endDate,
			name: "UPPERCASE"
		};
		const mapper = new MapperBuilder()
			.translate(["startDate", "endDate"], date => date.toISOString())
			.translate("name", name => name.toUpperCase())
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	it("should translate and rename values", () => {
		const startDate = "2020-01-01T00:00:00.000Z";
		const endDate = "2020-01-02T00:00:00.000Z";
		const input = {
			startDate: new Date(startDate),
			endDate: new Date(endDate)
		};
		const expectedOutput = {
			start: startDate,
			end: endDate
		};
		const mapper = new MapperBuilder()
			.translate({ startDate: "start", endDate: "end"}, date => date.toISOString())
			.build();

		// when
		const output = mapper.map(input);

		// then
		expect(output).to.deep.equal(expectedOutput);
	});

	describe("when working with collections", () => {
		it("should translate arrays", () => {
			// given
			const input = {
				numbers: [1, 1, 2, 3, 5, 8, 13],
				alsoNumbers: [1, 2, 4, 8, 16, 32],
				letters: ["A", "B", "C"]
			};
			const expectedOutput = {
				numbers: ["1", "1", "2", "3", "5", "8", "13"],
				alsoNumbers: ["1", "2", "4", "8", "16", "32"],
				letters: ["a", "b", "c"]
			};
			const mapper = new MapperBuilder()
				.translateEach(["numbers", "alsoNumbers"], num => num.toString())
				.translateEach("letters", letter => letter.toLowerCase())
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should translate and rename arrays", () => {
			// given
			const input = {
				numbers: [1, 1, 2, 3, 5, 8, 13],
				alsoNumbers: [1, 2, 4, 8, 16, 32]
			};
			const expectedOutput = {
				foo: ["1", "1", "2", "3", "5", "8", "13"],
				bar: ["1", "2", "4", "8", "16", "32"]
			};
			const mapper = new MapperBuilder()
				.translateEach({ numbers: "foo", alsoNumbers: "bar" }, num => num.toString())
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should map arrays", () => {
			// given
			const input = {
				gravityRanges: [{ min: 1040, max: 1060 }, { min: 1080, max: 1100 }],
				ibuRanges: [{ min: 20, max: 40 }, { min: 60, max: 100 }],
				srmRanges: [{ min: 2, max: 6 }, { min: 35, max: 40 }]
			};
			const expectedOutput = {
				gravityRanges: [{ min: "1040", max: "1060" }, { min: "1080", max: "1100" }],
				ibuRanges: [{ min: "20", max: "40" }, { min: "60", max: "100" }],
				srmRanges: [{ min: "2", max: "6" }, { min: "35", max: "40" }]
			};
			const minMaxMapper = new MapperBuilder()
				.translate(["min", "max"], value => value.toString())
				.build();
			const mapper = new MapperBuilder()
				.mapEach(["gravityRanges", "ibuRanges"], minMaxMapper)
				.mapEach("srmRanges", minMaxMapper)
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should map and rename arrays", () => {
			// given
			const input = {
				gravityRanges: [{ min: 1040, max: 1060 }, { min: 1080, max: 1100 }],
				ibuRanges: [{ min: 20, max: 40 }, { min: 60, max: 100 }]
			};
			const expectedOutput = {
				gravity: [{ min: "1040", max: "1060" }, { min: "1080", max: "1100" }],
				ibu: [{ min: "20", max: "40" }, { min: "60", max: "100" }]
			};
			const minMaxMapper = new MapperBuilder()
				.translate(["min", "max"], value => value.toString())
				.build();
			const mapper = new MapperBuilder()
				.mapEach({ gravityRanges: "gravity", ibuRanges: "ibu" }, minMaxMapper)
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should translate Sets", () => {
			// given
			const input = {
				numbers: new Set([1, 2, 3, 5, 8, 13]),
				alsoNumbers: new Set([1, 2, 4, 8, 16, 32]),
				letters: new Set(["A", "B", "C"])
			};
			const expectedOutput = {
				numbers: new Set(["1", "2", "3", "5", "8", "13"]),
				alsoNumbers: new Set(["1", "2", "4", "8", "16", "32"]),
				letters: new Set(["a", "b", "c"])
			};
			const mapper = new MapperBuilder()
				.translateEach(["numbers", "alsoNumbers"], num => num.toString())
				.translateEach("letters", letter => letter.toLowerCase())
				.build();

			// when
			const output: any = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
			expect(output.numbers).to.be.instanceOf(Set);
		});

		it("should map Sets", () => {
			// given
			const input = {
				gravityRanges: new Set([{ min: 1040, max: 1060 }, { min: 1080, max: 1100 }]),
				ibuRanges: new Set([{ min: 20, max: 40 }, { min: 60, max: 100 }]),
				srmRanges: new Set([{ min: 2, max: 6 }, { min: 35, max: 40 }])
			};
			const expectedOutput = {
				gravityRanges: new Set([{ min: "1040", max: "1060" }, { min: "1080", max: "1100" }]),
				ibuRanges: new Set([{ min: "20", max: "40" }, { min: "60", max: "100" }]),
				srmRanges: new Set([{ min: "2", max: "6" }, { min: "35", max: "40" }])
			};
			const minMaxMapper = new MapperBuilder()
				.translate(["min", "max"], value => value.toString())
				.build();
			const mapper = new MapperBuilder()
				.mapEach(["gravityRanges", "ibuRanges"], minMaxMapper)
				.mapEach("srmRanges", minMaxMapper)
				.build();

			// when
			const output: any = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
			expect(output.gravityRanges).to.be.instanceOf(Set);
		});

		it("should translate Maps", () => {
			// given
			const input = {
				numbers: new Map([["0", 1], ["1", 1], ["2", 2], ["3", 3], ["4", 5], ["5", 8], ["6", 13]]),
				alsoNumbers: new Map([["0", 1], ["1", 2], ["2", 4], ["3", 8], ["4", 16], ["5", 32]]),
				letters: new Map([["0", "A"], ["1", "B"], ["2", "C"]])
			};
			const expectedOutput = {
				numbers: new Map([["0", "1"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "5"], ["5", "8"], ["6", "13"]]),
				alsoNumbers: new Map([["0", "1"], ["1", "2"], ["2", "4"], ["3", "8"], ["4", "16"], ["5", "32"]]),
				letters: new Map([["0", "a"], ["1", "b"], ["2", "c"]])
			};
			const mapper = new MapperBuilder()
				.translateEach(["numbers", "alsoNumbers"], num => num.toString())
				.translateEach("letters", letter => letter.toLowerCase())
				.build();

			// when
			const output: any = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
			expect(output.numbers).to.be.instanceOf(Map);
		});

		it("should map Maps", () => {
			// given
			const input = {
				gravityRanges: new Map([["pils", { min: 1040, max: 1060 }], ["ipa", { min: 1080, max: 1100 }]]),
				ibuRanges: new Map([["pils", { min: 20, max: 40 }], ["ipa", { min: 60, max: 100 }]]),
				srmRanges: new Map([["pils", { min: 2, max: 6 }], ["ipa", { min: 35, max: 40 }]])
			};
			const expectedOutput = {
				gravityRanges: new Map([["pils", { min: "1040", max: "1060" }], ["ipa", { min: "1080", max: "1100" }]]),
				ibuRanges: new Map([["pils", { min: "20", max: "40" }], ["ipa", { min: "60", max: "100" }]]),
				srmRanges: new Map([["pils", { min: "2", max: "6" }], ["ipa", { min: "35", max: "40" }]])
			};
			const minMaxMapper = new MapperBuilder()
				.translate(["min", "max"], value => value.toString())
				.build();
			const mapper = new MapperBuilder()
				.mapEach(["gravityRanges", "ibuRanges"], minMaxMapper)
				.mapEach("srmRanges", minMaxMapper)
				.build();

			// when
			const output: any = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
			expect(output.gravityRanges).to.be.instanceOf(Map);
		});
	});

	describe("when explicitly specifying the output type", () => {
		class Output {
			private value: string = "?";
			public getValue(): string {
				return this.value;
			}
			constructor() {}
		}

		it("should allow using a factory", () => {
			// given
			const input = {
				amount: 10,
				unit: "KILOGRAM"
			};
			const mapper = new MapperBuilder<any, Output>()
				.transform((inputObject: any, outputObject: any) => {
					outputObject.value = [inputObject.amount.toString(), inputObject.unit].join(" ");
				})
				.outputFactory(() => new Output())
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.be.instanceOf(Output);
			expect((output as Output).getValue()).to.equal("10 KILOGRAM");
		});

		it("should allow using a type", () => {
			// given
			const input = {
				amount: 10,
				unit: "KILOGRAM"
			};
			const mapper = new MapperBuilder<any, Output>()
				.transform((inputObject: any, outputObject: any) => {
					outputObject.value = [inputObject.amount.toString(), inputObject.unit].join(" ");
				})
				.outputType(Output)
				.build();

			// when
			const output: Output = mapper.map(input) as Output;

			// then
			expect(output).to.be.instanceOf(Output);
			expect(output.getValue()).to.equal("10 KILOGRAM");
		});
	});

	describe("missing key behaviour", () => {
		it("should throw by default", () => {
			const mapper = new MapperBuilder()
				.copy("foo")
				.build();

			expect(() => mapper.map({}))
				.to.throw(MissingKeyError, "The key \"foo\" does not exist on the input object.");
		});

		it("should add mapper name to thrown error", () => {
			const mapper = new MapperBuilder()
				.copy("foo")
				.build("FooMapper");

			expect(() => mapper.map({}))
				.to.throw(MissingKeyError, "FooMapper: The key \"foo\" does not exist on the input object.");
		});

		it("should allow ignoring keys", () => {
			// given
			const input = {
				foo: "bar"
			};
			const expectedOutput = input;

			const mapper = new MapperBuilder()
				.ignoreMissing().copy(["foo", "qux"])
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should allow alternating between behaviours", () => {
			const mapper = new MapperBuilder()
				.ignoreMissing().copy("foo")
				.throwOnMissing().copy("qux")
				.build();

			expect(() => mapper.map({}))
				.to.throw(MissingKeyError, "The key \"qux\" does not exist on the input object.");
		});
	});

	describe("when passing Options", () => {
		it("should replace missing keys with defaults", () => {
			// given
			const expectedOutput = {
				bat: "man",
				super: "man",
				cat: "woman"
			};

			const mapper = new MapperBuilder()
				.copy(["bat", "super"], { default: "man" })
				.copy("cat", { default: "woman" })
				.build();

			// when
			const output = mapper.map({});

			// then
			expect(output).to.deep.equal(expectedOutput);
		});

		it("should allow value validation", () => {
			// given
			const input = {
				foo: "bar",
				super: ["man", "woman"]
			};
			const expectedOutput = input;
			const validate = sinon.spy();
			const mapper = new MapperBuilder()
				.copy("foo", { validate })
				.translateEach("super", val => val, { validate })
				.build();

			// when
			const output = mapper.map(input);

			// then
			expect(output).to.deep.equal(expectedOutput);
			expect(validate).to.have.been.calledThrice;
		});

		it("should throw on failed validation", () => {
			const input = {
				foo: "bar"
			};
			const validate = (value: any) => {
				if (value !== "hello") throw new Error("Oops!");
			};
			const mapper = new MapperBuilder()
				.copy("foo", { validate })
				.build();

			expect(() => mapper.map(input))
				.to.throw(Error, "Oops!");
		});

		it("should add mapper name to thrown errors", () => {
			const input = {
				foo: "bar"
			};
			const validate = (value: any) => {
				if (value !== "hello") throw new Error("Oops!");
			};
			const mapper = new MapperBuilder()
				.copy("foo", { validate })
				.build("FooMapper");

			expect(() => mapper.map(input))
				.to.throw(Error, "FooMapper: Oops!");
		});
	});
});
